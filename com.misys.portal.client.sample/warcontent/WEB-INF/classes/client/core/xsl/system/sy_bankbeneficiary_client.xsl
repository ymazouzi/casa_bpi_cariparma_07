<?xml version="1.0" encoding="UTF-8"?>
<!--
##########################################################
Templates for : TO DO : CANCEL + PASSBACK ENTITY

 Bank Beneficiary (On customer side) Screen, System Form.

Copyright (c) 2000-2013 Misys (http://www.misys.com),
All Rights Reserved. 
##########################################################
-->
<xsl:stylesheet 
		version="1.0" 
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
		xmlns:localization="xalan://com.misys.portal.common.localization.Localization"
		xmlns:securityCheck="xalan://com.misys.portal.common.security.GTPSecurityCheck"
		xmlns:utils="xalan://com.misys.portal.common.tools.Utils"
		xmlns:defaultresource="xalan://com.misys.portal.common.resources.DefaultResourceProvider"
		exclude-result-prefixes="localization securityCheck defaultresource utils">
		
 <!--
  Main Details of the Bank Beneficiary
  -->
 <xsl:template name="bank-beneficiary-details">
  <xsl:param name="nodeName"/>
  <!-- add title if popup-->
  <xsl:if test="$formname != ''">
   <xsl:choose>
	<xsl:when test="$nodeName ='static_bank'">
	 <xsl:value-of select="localization:getGTPString($language, 'OpenAddBankCSF')"/>
	</xsl:when>
	<xsl:when test="$nodeName ='static_beneficiary'">
	 <xsl:value-of select="localization:getGTPString($language, 'OpenAddBeneficiaryCSF')"/>
    </xsl:when>				
   </xsl:choose>				
  </xsl:if>
  
  <xsl:call-template name="fieldset-wrapper">
   <xsl:with-param name="legend">XSL_HEADER_MAIN_DETAILS</xsl:with-param>
   <xsl:with-param name="content">
    <!-- Entity -->
 	<xsl:choose>
     <xsl:when test="entities">
      <xsl:call-template name="entity-field">
       <xsl:with-param name="button-type">system-entity</xsl:with-param>
       <xsl:with-param name="entity-label">XSL_JURISDICTION_ENTITY</xsl:with-param>
       <!-- TODO This is a temp solution to entity problem -->
       <xsl:with-param name="required">
        <xsl:choose>
        <xsl:when test="count(entities)=0">N</xsl:when>
        <xsl:otherwise>Y</xsl:otherwise>
        </xsl:choose>
       </xsl:with-param>
      </xsl:call-template>
 	 </xsl:when>
   	 <xsl:otherwise>
   	  <xsl:call-template name="hidden-field">
       <xsl:with-param name="name">entity</xsl:with-param>
       <xsl:with-param name="value">*</xsl:with-param>
      </xsl:call-template>
     </xsl:otherwise>
   	</xsl:choose>
 	
 	<!-- Abbreviated Name -->   
    <xsl:call-template name="input-field">
     <xsl:with-param name="label">XSL_JURISDICTION_ABREVIATED_NAME</xsl:with-param>
     <xsl:with-param name="name">abbv_name</xsl:with-param>
     <xsl:with-param name="regular-expression">[a-zA-Z0-9]{1,35}</xsl:with-param>
     <xsl:with-param name="required">Y</xsl:with-param>
     <xsl:with-param name="readonly"><xsl:if test="abbv_name != ''">Y</xsl:if></xsl:with-param>
    </xsl:call-template>
     
 	<!-- Name -->    
	<xsl:call-template name="input-field">
     <xsl:with-param name="label">XSL_JURISDICTION_NAME</xsl:with-param>
     <xsl:with-param name="name">name</xsl:with-param>
     <xsl:with-param name="required">Y</xsl:with-param>
    </xsl:call-template>
    
    <!-- Collaboration template to deal with security verifications -->
     <xsl:call-template name="collaboration-security-check">
     	 <xsl:with-param name="nodeName"><xsl:value-of select="$nodeName"/></xsl:with-param>
     </xsl:call-template>
      <xsl:if test="$nodeName = 'static_beneficiary'">
	 	<!-- Inclusion of Credit Note Product Sub Header-->
	 	<xsl:call-template name="fieldset-wrapper">
	    <xsl:with-param name="legend">MENU_CHANGE_PRODUCTS</xsl:with-param><!--TODO: Have to create localized header -->
	    <xsl:with-param name="legend-type">indented-header</xsl:with-param>
	    <xsl:with-param name="content">
			<xsl:call-template name="multichoice-field">
				<xsl:with-param name="name">access_credit_note_product</xsl:with-param>
				<xsl:with-param name="checked">N</xsl:with-param>
				<xsl:with-param name="type">checkbox</xsl:with-param>
				<xsl:with-param name="disabled">N</xsl:with-param>
				<xsl:with-param name="label">XSL_CREDIT_NOTE_PRODUCT</xsl:with-param>
			</xsl:call-template>
			<!-- TMA Proprietary Id Field only for IO products -->
			<!-- TMA Proprietary Id field appears Only for beneficiary and Non-invoice Financing programme,
				 and with counterparty collaboration disabled and Non-credit note products -->
			<xsl:if test="contains($nodeName,'static_beneficiary') and $companyType and $companyType='03'"> 
				<xsl:if test="securityCheck:hasCompanyPermission($rundata,'io_access') or securityCheck:hasCompanyPermission($rundata,'ea_access')">
					<xsl:call-template name="multichoice-field">
						<xsl:with-param name="name">prtry_id_type_flag</xsl:with-param>
						<xsl:with-param name="checked">N</xsl:with-param>
						<xsl:with-param name="type">checkbox</xsl:with-param>
						<xsl:with-param name="disabled">N</xsl:with-param>
						<xsl:with-param name="label">XSL_TMA_PROPRIETARY_ID</xsl:with-param>
					</xsl:call-template>
					<div id="prtry_id_type_div" >
				   	 <xsl:call-template name="input-field">
				      <xsl:with-param name="label">XSL_PROPRIETARY_ID_AND_TYPE_LABEL</xsl:with-param>
				      <xsl:with-param name="name">prtry_id_type</xsl:with-param>
				      <xsl:with-param name="maxsize">71</xsl:with-param>
				      <xsl:with-param name="required">Y</xsl:with-param>
				     </xsl:call-template>
					</div>
					<xsl:call-template name="hidden-field">
						<xsl:with-param name="name">prtry_id_type_separator</xsl:with-param>
						<xsl:with-param name="value"><xsl:value-of select="substring(defaultresource:getResource('TMA_PROPRIETARY_ID_SEPARATOR'), 1, 1)"/></xsl:with-param>
					</xsl:call-template>
				</xsl:if>
			</xsl:if>
	    </xsl:with-param>
	   </xsl:call-template>
    </xsl:if>
 	<!-- Template in "system_common.xsl":
 		To deal with Main details (Tabs for SWIFT and POSTAL address -->
   <xsl:call-template name="address-details">
   <!--[CUSTUMIZATION-SPECIFICATION] We changed the addressMandatory from Y to N to make this field non-mandatory -->
	   <xsl:with-param name="addressMandatory"><xsl:choose>
		 <xsl:when test="securityCheck:hasPermission($rundata,'swift_adress_non_mandatory')">N</xsl:when>
		<xsl:otherwise>Y</xsl:otherwise>
	</xsl:choose></xsl:with-param> 
   <!--[CUSTUMIZATION-SPECIFICATION] This line makes the PostalAddress fields mandatory in static_bank with permission-->
	<xsl:with-param name="PostaladdressMandatory">
	<xsl:choose>
		 <xsl:when test="contains($nodeName,'static_bank') and securityCheck:hasPermission($rundata,'add_bank_fields_mandatory')">Y</xsl:when>
		<xsl:otherwise>N</xsl:otherwise>
	</xsl:choose>
	</xsl:with-param>
	
   </xsl:call-template>
  </xsl:with-param>
 </xsl:call-template>
</xsl:template>		
		
 <!--
 	Bank Beneficiary Others Details 
  -->
  <xsl:template name="bankBeneficiary-other-details">
   <xsl:param name="nodeName"/>
  	
   <xsl:call-template name="fieldset-wrapper">
   	<xsl:with-param name="legend">XSL_HEADER_OTHER_DETAILS</xsl:with-param>
   	<xsl:with-param name="content">
   	 <xsl:call-template name="input-field">
      <xsl:with-param name="label">XSL_JURISDICTION_CONTACT_NAME</xsl:with-param>
      <xsl:with-param name="name">contact_name</xsl:with-param>
      <xsl:with-param name="size">30</xsl:with-param>
      <xsl:with-param name="maxsize">30</xsl:with-param>
     </xsl:call-template>	
     <xsl:call-template name="input-field">
      <xsl:with-param name="label">XSL_JURISDICTION_PHONE</xsl:with-param>
      <xsl:with-param name="name">phone</xsl:with-param>
      <xsl:with-param name="size">32</xsl:with-param>
      <xsl:with-param name="maxsize">32</xsl:with-param>
     </xsl:call-template>
     <xsl:call-template name="input-field">
      <xsl:with-param name="label">XSL_JURISDICTION_FAX</xsl:with-param>
      <xsl:with-param name="name">fax</xsl:with-param>
      <xsl:with-param name="size">32</xsl:with-param>
      <xsl:with-param name="maxsize">32</xsl:with-param>
     </xsl:call-template>
     <xsl:call-template name="input-field">
      <xsl:with-param name="label">XSL_JURISDICTION_TELEX</xsl:with-param>
      <xsl:with-param name="name">telex</xsl:with-param>
      <xsl:with-param name="size">32</xsl:with-param>
      <xsl:with-param name="maxsize">32</xsl:with-param>
     </xsl:call-template>
     <xsl:choose>
	  <xsl:when test="contains($nodeName,'static_bank')">
    	<xsl:call-template name="input-field">
      	 <xsl:with-param name="label">XSL_JURISDICTION_BIC_CODE</xsl:with-param>
      	 <xsl:with-param name="name">iso_code</xsl:with-param>
      	 <xsl:with-param name="size">11</xsl:with-param>
       	 <xsl:with-param name="maxsize">11</xsl:with-param>
         <xsl:with-param name="uppercase">Y</xsl:with-param>
         <xsl:with-param name="fieldsize">small</xsl:with-param>
          <!--[CUSTUMIZATION_SPECIFICATION] require to BIC with a "add_bank_fields_mandatory" permission -->
         <xsl:with-param name="required">
		 <xsl:choose>
		 <xsl:when test="securityCheck:hasPermission($rundata,'add_bank_fields_mandatory')">Y</xsl:when>
		 <xsl:otherwise>N</xsl:otherwise>
	     </xsl:choose>
		 </xsl:with-param>
    	</xsl:call-template>
      </xsl:when>		
	  <xsl:when test="$nodeName ='static_beneficiary'">
		<xsl:call-template name="input-field">
		 <xsl:with-param name="label">XSL_JURISDICTION_ADDRESS_BEI</xsl:with-param>
		 <xsl:with-param name="name">bei</xsl:with-param>
		 <xsl:with-param name="size">11</xsl:with-param>
		 <xsl:with-param name="maxsize">11</xsl:with-param>
         <xsl:with-param name="uppercase">Y</xsl:with-param>
         <xsl:with-param name="fieldsize">small</xsl:with-param>
		</xsl:call-template>
	   </xsl:when>
	  </xsl:choose>
	  <xsl:call-template name="input-field">
	   <xsl:with-param name="label">XSL_JURISDICTION_EMAIL</xsl:with-param>
	   <xsl:with-param name="name">email</xsl:with-param>
	   <xsl:with-param name="size">24</xsl:with-param>
 	   <xsl:with-param name="maxsize">255</xsl:with-param>
 	   <xsl:with-param name="required">
		<xsl:choose>
       		<xsl:when test="$counterparty_email_required = 'true' and securityCheck:hasPermission($rundata,'email_non_mandatory')">N</xsl:when>
            <xsl:otherwise>Y</xsl:otherwise>
     	</xsl:choose>
	   </xsl:with-param> 
	  </xsl:call-template>
      <xsl:call-template name="input-field">
	   <xsl:with-param name="label">XSL_JURISDICTION_WEB</xsl:with-param>
	   <xsl:with-param name="name">web_address</xsl:with-param>
	   <xsl:with-param name="size">24</xsl:with-param>
	   <xsl:with-param name="maxsize">40</xsl:with-param>
      </xsl:call-template>	
   	 </xsl:with-param>
   	</xsl:call-template>
  </xsl:template>		
		
		
</xsl:stylesheet>